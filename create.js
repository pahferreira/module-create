const fs = require('fs')

module.exports = createModule = async nome => {
  const content = {
    router: `import express from 'express'
import ${nome.charAt(0).toUpperCase() +
      nome.slice(1)}Controller from './controller'
const router = express.Router()
          
router.get('/all', controller.getAll)
          
export default router`,
    controller: `import ${nome.charAt(0).toUpperCase() +
      nome.slice(1)} from './model.js'
            
const getAll = (req, res) => {
  //Insert Logic Here
}
            
export { getAll }`,
    model: `import mongoose from 'mongoose'
const Schema = mongoose.Schema
            
const ${nome.charAt(0).toUpperCase() + nome.slice(1)} = new Schema({
  //Create your Mongoose Schema here.
})
            
export default mongoose.model('${nome.charAt(0).toUpperCase() +
      nome.slice(1)}', ${nome.charAt(0).toUpperCase() + nome.slice(1)}Schema)`,
    repository: ''
  }
  fs.mkdir(`./${nome}`, err => {
    if (err) {
      console.log(err)
    } else {
      console.log(`Criando Módulo ${nome}`)
    }
  })
  let writeStream
  Object.entries(content).forEach(([key, value]) => {
    writeStream = fs.createWriteStream(`./${nome}/${key}.js`)
    writeStream.write(value)
    writeStream.end()
  })
}

//createModule(process.argv[2])
