import express from 'express'
import nome_moduloController from './controller'
const router = express.Router()

router.get('/all', nome_moduloController.getAll)

export default router
