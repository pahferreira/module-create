# Como Utilizar

- Para startar um módulo seguindo o padrão:

```
node create-module [nome-do-modulo]
```

- Para startar um módulo seguindo os arquivos de template:

```
node create-module [nome-do-modulo] template
```
