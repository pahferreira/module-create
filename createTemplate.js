const fs = require('fs')
const readline = require('readline')

const createFile = (moduleName, file) => {
  let lineReader = readline.createInterface({
    input: fs.createReadStream(`./template/${file}.js`)
  })
  let writeStream = fs.createWriteStream(`./${moduleName}/${file}.js`)
  let count = 0
  lineReader.on('line', line => {
    count++
    if (line.includes('nome_modulo')) {
      line = line
        .split('nome_modulo')
        .join(moduleName.charAt(0).toUpperCase() + moduleName.slice(1))
    }
    writeStream.write(line)
    writeStream.write('\n')
  })
}

module.exports = createModule = nome => {
  fs.mkdir(`./${nome}`, err => {
    if (err) {
      console.log(err)
    } else {
      console.log(`Criando Módulo ${nome}...`)
    }
  })
  let filesName = ['router', 'controller', 'model', 'repository']
  filesName.forEach(fileName => createFile(nome, fileName))
}
