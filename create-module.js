const create = require('./create')
const createTemplate = require('./createTemplate')

let moduleName = process.argv[2]
if (moduleName.match(/[^\w\*]/g)) {
  throw 'Only alphanumerics characters.'
}
process.argv[3] == 'template' ? createTemplate(moduleName) : create(moduleName)
